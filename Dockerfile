FROM node:latest

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

RUN apt-get update && apt-get install -y git

RUN apt-get install -y maven

RUN apt-get install -y unzip
RUN apt-get install -y wget
RUN wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.6.0.2311-linux.zip
RUN unzip sonar-scanner-cli-4.6.0.2311-linux.zip -d /opt
RUN rm sonar-scanner-cli-4.6.0.2311-linux.zip
ENV PATH="$PATH:/opt/sonar-scanner-4.6.0.2311-linux/bin"

# Expose port 80
EXPOSE 3000

# Start the React app
CMD ["npm", "run", "start"]